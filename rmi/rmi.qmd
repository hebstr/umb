---
title: "RMI-IMPLANTS"

author: 
  name: "Julien ELICES-DIEZ"
  email: "ju.diez@protonmail.com"

date: 2024-10-26

filters: 
  - add-code-files

format:
  html:
    lang: fr
    embed-resources: true
    toc: true
    toc-depth: 4
    toc-expand: true
    number-sections: true
    number-depth: 4
    colorlinks: true
    code-copy: true
    code-link: true
    code-tools: true
    code-fold: true
    html-table-processing: none
    theme:
      - cosmo
      - ../_css/style.scss
    grid:
      sidebar-width: 0px
      body-width: 1000px
      margin-width: 400px

lightbox: true

knitr: 
  opts_chunk:
    collapse: true
    comment: "#>"
    echo: false
    message: false
    warning: false

editor_options: 
  chunk_output_type: console
---

```{r setup, include=FALSE}
library(knitr)

quiet <- TRUE

source("scripts/_setup.R")
auto_exec()
```

# Maps

::: {add-from=scripts/fig_map_slide.R code-line-numbers="true" code-filename="fig_map_slide.R"}
```r
```
:::

::: {add-from=scripts/fig_map_publi.R code-line-numbers="true" code-filename="fig_map_publi.R"}
```r
```
:::

## Répondants

::: panel-tabset

# slide

```{r}
#| out-width: 80%
#| fig-align: center

include_graphics("output/fig_map_reponse_slide.png")
```

# publi

```{r}
#| out-width: 80%
#| fig-align: center

include_graphics("output/fig_map_reponse_publi.png")
```

Nombre de répondants selon le département et le secteur d'activité.

Sur 181 répondants, 2 n'ont indiqué ni leur département ni leur secteur d'activité. 5 répondants ont indiqué leur secteur d'activité (3 CLCC + 2 SP) sans indiquer leur département. Non représentés : Algérie (2 CH), Autre (1 CLCC), Belgique (1 CLCC), Martinique (1 CH), Monaco (1 CH) et Île Maurice (1 SP).

:::

## Positionnement de l'implant

::: panel-tabset

# slide

```{r}
include_graphics("output/fig_map_position_slide.png")
```

# publi

Positionnement de l'implant après une reconstruction mammaire immédiate.

```{r}
include_graphics("output/fig_map_position_publi.png")
```

Nombre de répondants selon le département et l'activité en centre de lutte contre le cancer (CLCC), centre hospitalier/CHU (CH) ou secteur privé (SP).

(**A**) Sur 78 répondants, 1 n'a indiqué ni son département ni son secteur d'activité. 1 répondant a indiqué son secteur d'activité (1 CLCC) sans indiquer son département. Non représentés : Autre (1 CLCC) et Belgique (1 CLCC).

(**B**) Sur 85 répondants, 1 n'a indiqué ni son département ni son secteur d'activité. 3 répondants ont indiqué leur secteur d'activité (2 CLCC + 1 SP) sans indiquer leur département. Non représentés : Autre (1 CLCC), Martinique (1 CH), Monaco (1 CH) et Île Maurice (1 SP).

:::

# Arguments

::: {add-from=scripts/fig_args.R code-line-numbers="true" code-filename="fig_args.R"}
```r
```
:::

## Pour la pose prépectorale

::: panel-tabset

# slide

```{r}
#| layout-nrow: 2
#| out-width: 70%
#| fig-align: center

include_graphics("output/fig_args_pour_pre.slide.png")

include_graphics("output/fig_args_contre_retro.slide.png")
```

# publi

Si vous réalisez **« toujours ou souvent »** des RMI par implants en position [prépectorale]{.cb1}, pourquoi ?

```{r}
#| out-width: 70%
#| fig-align: center

include_graphics("output/fig_args_pour_pre.publi.png")
```

Si vous réalisez **« rarement ou jamais »** des RMI par implants en position [rétropectorale]{.cb2}, pourquoi ?

```{r}
#| out-width: 70%
#| fig-align: center

include_graphics("output/fig_args_contre_retro.publi.png")
```

:::

## Pour la pose rétropectorale

::: panel-tabset

# slide

```{r}
#| layout-nrow: 2
#| out-width: 70%
#| fig-align: center

include_graphics("output/fig_args_contre_pre.slide.png")

include_graphics("output/fig_args_pour_retro.slide.png")
```

# publi

Si vous réalisez **« rarement ou jamais »** des RMI par implants en position [prépectorale]{.cb1}, pourquoi ?

```{r}
#| out-width: 70%
#| fig-align: center

include_graphics("output/fig_args_contre_pre.publi.png")
```

Si vous réalisez **« toujours ou souvent »** des RMI par implants en position [rétropectorale]{.cb2}, pourquoi ?

```{r}
#| out-width: 70%
#| fig-align: center

include_graphics("output/fig_args_pour_retro.publi.png")
```

:::

# Profil et expérience des répondants

::: {add-from=scripts/fig_profil_slide.R code-line-numbers="true" code-filename="fig_profil_slide.R"}
```r
```
:::

::: {add-from=scripts/fig_profil_publi.R code-line-numbers="true" code-filename="fig_profil_publi.R"}
```r
```
:::

## Répondants

```{r}
#| out-width: 90%
#| fig-align: center

include_graphics("output/fig_profil_reponse.png")
```

## Positionnement de l'implant

::: panel-tabset

# slide

### Selon l'expérience

```{r}
#| out-width: 70%
#| fig-align: center

include_graphics("output/fig_profil_position.experience.png")
```

### Selon le volume

```{r}
#| out-width: 70%
#| fig-align: center

include_graphics("output/fig_profil_position.volume.png")
```

# publi

Positionnement de l'implant après une reconstruction mammaire immédiate.

```{r}
#| out-width: 70%
#| fig-align: center

include_graphics("output/fig_profil_position_publi.png")
```

Nombre de répondants déclarant positionner l'implant **« toujours ou souvent »** en position [prépectorale]{.cb1} ou [rétropectorale]{.cb2}.

(**A**) Selon la durée d'exercice.
 
(**B**) Selon le volume d'activité.

:::

# Positionnement de l'implant

::: {add-from=scripts/fig_prothese.R code-line-numbers="true" code-filename="fig_prothese.R"}
```r
```
:::

```{r}
#| out-width: 90%
#| fig-align: center

include_graphics("output/fig_prothese.png")
```

::: {add-from=scripts/fig_cross.R code-line-numbers="true" code-filename="fig_cross.R"}
```r
```
:::

```{r}
#| out-width: 70%
#| fig-align: center

include_graphics("output/fig_cross.png")
```

# Pose de matrice

::: {add-from=scripts/fig_matrice.R code-line-numbers="true" code-filename="fig_matrice.R"}
```r
```
:::

```{r}
#| out-width: 60%
#| fig-align: center

include_graphics("output/fig_matrice.png")
```
