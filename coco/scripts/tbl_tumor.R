### DATA -------------------------------------------------------------------------

.tumor <- 
df_id |> 
  select(tfi_median, metachr, nephrec, tumor, furhman_gr, sarcoma_ft) |> 
  modify_if(is.factor, ~ fct_na_level_to_value(., opts$labs$missing))

### TABLE ------------------------------------------------------------------------

.tumor |> 
  tbl_summary(by = tfi_median,
              statistic = opts$vars(.tumor)$stat,
              digits = opts$digits,
              missing = "ifany",
              missing_text = opts$labs$missing) |> 
  add_p(test = opts$vars(.tumor)$test,
        pvalue_fun = opts$pvalue$format) |> 
  add_stat_label(label = opts$vars(.tumor)$label) |> 
  gtsum_format(label_header = opts$labs$header,
               label_overall = opts$labs$overall) |> 
  gt_format(width = 600,
            !!!opts$gt) |> 
  add_note(vars = "nephrec",
           note = 
             "All metachronous patients underwent a nephrectomy (localized stage). 
             6 more patients underwent a cytoreductive nephrectomy.") |> 
  add_note(vars = "tumor",
           note = 
             "Pathologic tumor staging. 
             Among patients who underwent nephrectomy.") |> 
  inject() |> 
  easy_out("tbl_tumor")
