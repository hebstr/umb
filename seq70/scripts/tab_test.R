.t_base <- 
lst(data =
      df_id |> 
        select("chemo_schema", "sexe", "age", "bmi_cat", "who", "plmedic",
               "oncog_ass", "denut", "charlson", "g8score", "albu"),
    d = easy_descr(data))

.t_base$data |> 
  tbl_summary(by = chemo_schema,
              type = .t_base$d$qt$vars$total ~ "continuous",
              statistic = 
                list(.t_base$d$qt$vars$para ~ .t_base$d$qt$stat$mean,
                     .t_base$d$qt$vars$nonpara ~ .t_base$d$qt$stat$median,
                     all_categorical() ~ .t_base$d$ql$stat$n),
              digits = 
                list(all_continuous() ~ 1,
                     all_categorical() ~ c(0, 1)),
              missing = "ifany",
              missing_text = "Missing data") |> 
  add_p(test = 
          list(all_continuous() ~ "quanti.test",
               all_categorical() ~ "quali.test"),
        pvalue_fun = .s$pvalue$format) |> 
  add_stat_label(label =
                   list(.t_base$d$qt$vars$para ~ 
                          str_cap(tolower, names(.t_base$d$qt$stat$mean)),
                        .t_base$d$qt$vars$nonpara ~ 
                          str_cap(tolower, names(.t_base$d$qt$stat$median)))) |> 
  gtsum_format(label_header = .s$labs$header,
               label_overall = .s$labs$overall,
               bold_p = .s$pv$seuil) |> 
  gt_format(title = "**Table X.** Baseline characteristics.",
            note_global = 
              glue("P-values were bolded if under 
                   or equal to {.s$pv$seuil}."),
            width = 550,
            !!!opts$tab) |> 
  inject() |> 
  easy_out("tab_base")

